import irctokens
import requests
import bugzilla
from bs4 import BeautifulSoup
def bug(*, line, _send, command, gentoo_bugzilla_api_key, **kwargs):
    channel_reply = line.params[0]
    command.split()
    if command[5:].isdigit():
        bug_id = command[5:]
    else:
        bug_id = "error"
    if command[5:11] == "search" and len(command) > 12:
        url = f'https://bugs.gentoo.org/buglist.cgi?quicksearch={command[12:]}'
        reqs = requests.get(url)
        title = BeautifulSoup(reqs.text, 'html.parser')
        for title in title.find_all('title'):
            title.get_text
        b = bugzilla.Bugzilla(url="https://bugs.gentoo.org/rest/", api_key=gentoo_bugzilla_api_key)
        try:
            bug_id = b.quick_search(command[12:])['bugs'][0]['id']
        except:
            bug_id = "error"
    url = f'https://bugs.gentoo.org/{bug_id}'
    reqs = requests.get(url)
    title = BeautifulSoup(reqs.text, 'html.parser')
    for title in title.find_all('title'):
        title.get_text()

    if title.get_text() == "404 Not Found":
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Invalid bug"])
        _send(to_send)
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"https://bugs.gentoo.org/{bug_id} | {title.get_text()}"])
        _send(to_send)
        print(f'https://bugs.gentoo.org/{command[5:]}')
