import irctokens
def op(*, line, _send, command, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "+o", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'op' command."])
        _send(to_send)
def deop(*, line, _send, command, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "-o", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'deop' command."])
        _send(to_send)
def voice(*, line, _send, command, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "+v", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'voice' command."])
        _send(to_send)
def devoice(*, line, _send, command, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "-v", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'devoice' command."])
        _send(to_send)
def kick(*, line, _send, command, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("KICK", [channel_reply, arg[1], arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'kick' command."])
        _send(to_send)

def version(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    print(line.params[0])
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: Barium: version 1.0, Gentoo GNU/Linux"])
    _send(to_send)
def code(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    print(line.params[0])
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: Barium: https://gitlab.com/rahul.sandhu/barium-bot"])
    _send(to_send)
def quit_irc(*, line, _send, owner, **kwargs):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Exiting"])
        _send(to_send)
        exit(99)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'quit' command."])
        _send(to_send)
